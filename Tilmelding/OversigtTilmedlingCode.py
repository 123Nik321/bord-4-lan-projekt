from Andet import gui
from wx.lib.pubsub import pub

class oversigt_tilmelding(gui.Oversigt_Tilmelding):
    def __init__(self, parent):
        gui.Oversigt_Tilmelding.__init__(self, parent)
        pub.subscribe(self.listenervindue, "afslut")
        pub.subscribe(self.listener, "TilmeldingTilLan")
        # Laver 5 kolonner i ListCtrl, med hver sit navn
        self.TilmedlingsList.InsertColumn(0, "Navn")
        self.TilmedlingsList.InsertColumn(1, "Hold")
        self.TilmedlingsList.InsertColumn(2, "Hardware")
        self.TilmedlingsList.InsertColumn(3, "Deltagere hele lan'et")
        self.TilmedlingsList.InsertColumn(4, "Watt forbrug i alt")

        # Indsæter et item, hvor det samlede watt forbrug vil stå
        self.TilmedlingsList.InsertItem(0, "I alt: ")

        #Rækkefølgen som vores data indsættes i, når data indsættes i ListCtrl
        self.index = 0

        # Laver en liste, som skal bruges til at beregne samlet pris og antal mad
        self.iAlt = [0]




    def listener(self, tilmelding):
        # Indsætter data i ListCtrl
        self.TilmedlingsList.InsertItem(self.index, tilmelding[0])
        self.TilmedlingsList.SetItem(self.index, 1, str(tilmelding[1]))
        self.TilmedlingsList.SetItem(self.index, 2, str(tilmelding[2]))
        self.TilmedlingsList.SetItem(self.index, 3, str(tilmelding[3]))
        self.TilmedlingsList.SetItem(self.index, 4, str(tilmelding[4]))
        # Index stiger med 1, så de næste dataer ligger øverst.
        self.index += 1


        # Tager den udregnede forbrug af watt, og indsætter i ListCtrl
        self.iAlt[0] += int(tilmelding[4])
        self.TilmedlingsList.SetItem(self.index, 4, str(self.iAlt[0]) + " W")



    def listenervindue(self):
        # Lukker vindue
        self.Hide()