from Andet import gui
from wx.lib.pubsub import pub
import csv


class tilmelding(gui.TilmeldingFrame):
    def __init__(self, parent):
        gui.TilmeldingFrame.__init__(self, parent)
        self.TilmeldingList = []
        #Giver filnavn
        self.fileName = "CsvFiler\RapportTilmelding.csv"

    def tilmeld( self, event ):

        #Laver liste
        self.TilmeldingListTilRapport = []
        self.TilmeldingListTilRapport.append("Navn: " + self.tilmeld_Navn.GetValue())
        self.TilmeldingListTilRapport.append("Hold: " + self.tilmeld_Hold.GetValue())
        self.TilmeldingListTilRapport.append("Hardware: " + self.tilmeld_hardware.GetValue())

        # Tilføjer data til TilmeldingList og tjekker længere nede, om man deltager eller ej, i de diverse turneringer
        self.TilmeldingList.append(self.tilmeld_Navn.GetValue())
        self.TilmeldingList.append(self.tilmeld_Hold.GetValue())
        self.TilmeldingList.append(self.tilmeld_hardware.GetValue())

        if self.deltager_helelan.IsChecked():
            self.TilmeldingList.append("Ja")
            self.TilmeldingListTilRapport.append("Deltagere under hele lan'et")
        else:
            self.TilmeldingList.append("Nej")
            self.TilmeldingListTilRapport.append("Deltager ikke under hele lan'et")

        self.TilmeldingList.append(self.tilmeld_watforbrug.GetValue())
        self.TilmeldingListTilRapport.append("Samlet watt forbrug: " + self.tilmeld_watforbrug.GetValue() + "W")


        # Sender listen(TilmeldingsList)
        pub.sendMessage("TilmeldingTilLan", tilmelding=self.TilmeldingList)


        with open(self.fileName, mode='a') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            csv_writer.writerow(self.TilmeldingListTilRapport)


        # Rydder listen
        self.TilmeldingList.clear()
        self.TilmeldingListTilRapport.clear()

    def anuller( self, event ):
        pub.sendMessage("afslut")
        #Lukker vindue
        self.Hide()