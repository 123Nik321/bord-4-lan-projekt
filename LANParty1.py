import wx
from Andet import gui
from Tilmelding.TilmeldingCode import tilmelding
from Indstillinger.IndstillingerCode import indstillinger
from Turnering.TurneringCode import turneringsFrame
from Turnering.OversigtTurneringCode import turnerings_oversigtFrame
from Turnering.IndstillingerForTurnering import turneringsIndstillingerFrame
from Mad.MadCode import madFrame
from Mad.OversigtMadCode import oversigt_mad
from Rapport.Rapport import RapportForTurnering
from Tilmelding.OversigtTilmedlingCode import oversigt_tilmelding


class mainFrame(gui.MainFrame):
    def __init__(self, parent):
        gui.MainFrame.__init__(self, parent)
        #Intializere de forskellige frames
        self.madFrame = madFrame(self)
        self.oversigt_mad = oversigt_mad(self)
        self.indstillinger = indstillinger(self)
        self.tilMelding = tilmelding(self)
        self.turnering = turneringsFrame(self)
        self.oversigt_turnering = turnerings_oversigtFrame(self)
        self.TurneringsIndstillinger = turneringsIndstillingerFrame(self)
        self.RapprtForTurnering = RapportForTurnering(self)
        self.oversigt_tilmelding = oversigt_tilmelding(self)


    #Åbner de forskellige vinduer
    def OpenTurnering( self, event ):
        self.turnering.Show()
        self.oversigt_turnering.Show()

    def close( self, event ):
        #Lukker programmet
        exit(0)

    def mad( self, event ):
        self.madFrame.Show()
        self.oversigt_mad.Show()

    def indstillinger( self, event ):
        self.indstillinger.Show()
        self.TurneringsIndstillinger.Show()

    def tilmelding( self, event ):
        self.tilMelding.Show()
        self.oversigt_tilmelding.Show()

    def OpenRapport( self, event ):
        self.RapprtForTurnering.Show()

app = wx.App(False)
frame = mainFrame(None)
frame.Show(True)
app.MainLoop()
