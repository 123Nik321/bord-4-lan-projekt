from Andet import gui
from wx.lib.pubsub import pub
import pickle
import csv

class madFrame(gui.MadFrame):
    def __init__(self, parent):
        gui.MadFrame.__init__(self, parent)
        self.ordre = []
        self.config = []
        self.fileNameMad = "CsvFiler\RapportMad.csv"
        self.filename = "Andet\config.ini"
        #rb = read binary, åbner filename
        infile = open(self.filename, "rb")
        #Indsætter data fra filen ind i listen(config)
        self.config = pickle.load(infile)
        #Lukker filen
        infile.close()
        #Indsætter priser på mad og drikke til labeltxt
        self.txtPizzaPris.SetLabelText(self.config[0] + ",- kr.")
        self.txtBurgerPris.SetLabelText(self.config[1] + ",- kr.")
        self.txtColaPris.SetLabelText(self.config[2] + ",- kr.")


    def bestil( self, event ):
        #Tilføjer mad bestillingen, ind i listen ordre
        self.ordre.append(self.nametxt.GetValue())
        self.ordre.append(self.pizza_choice.GetSelection())
        self.ordre.append(self.burger_choise.GetSelection())
        self.ordre.append(self.cola_choice.GetSelection())
        self.ordre.append(self.label_ialt.GetLabelText())
        #Sender denne liste(ordre) med beskeden bestilling, så hvis modtageren, modtager teksten bestillingen, får de listen med ordre.
        pub.sendMessage("bestilling", bestilling = self.ordre)

        #Laver liste
        self.ordreTilRapport = []
        self.ordreTilRapport.append("Navn: " + str(self.nametxt.GetValue()))
        self.ordreTilRapport.append("Pizza: " + str(self.pizza_choice.GetSelection()))
        self.ordreTilRapport.append("Burger: " + str(self.burger_choise.GetSelection()))
        self.ordreTilRapport.append("Cola: " + str(self.cola_choice.GetSelection()))
        self.ordreTilRapport.append("Pris i alt: " + str(self.label_ialt.GetLabelText()) + ",- kr.")

        with open(self.fileNameMad, mode='a') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            csv_writer.writerow(self.ordreTilRapport)

        #Rydder listen
        self.ordre.clear()
        self.ordreTilRapport.clear()

    def slut_mad( self, event ):
        #Lukker vindue, og sender besked om at der afsluttes(Den sendes til et andet vindue)
        pub.sendMessage("afslut")
        self.Hide()

    def samletPris(self, pizza, burger, cola):
        #Udregner priserne på madbestillingerne
        pizzaialt = float(pizza)*float(self.config[0])
        burgerialt = float(burger)*float(self.config[1])
        colaialt = float(cola)*float(self.config[2])

        prisialt = pizzaialt+burgerialt+colaialt

        #Retunere prisen.
        return prisialt
#her der laver vi en fungtion der regner prisen og skriver det ind i vores liste.
    def updateTotalPrice( self, event ):
        self.label_ialt.SetLabelText(str(self.samletPris(self.pizza_choice.GetSelection(), self.burger_choise.GetSelection(), self.cola_choice.GetSelection())))
