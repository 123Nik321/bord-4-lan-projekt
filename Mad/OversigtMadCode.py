from Andet import gui
from wx.lib.pubsub import pub


class oversigt_mad(gui.Oversigt_mad):
    def __init__(self, parent):
        gui.Oversigt_mad.__init__(self, parent)
        pub.subscribe(self.listenervindue, "afslut")
        pub.subscribe(self.listener, "bestilling")
        # Laver 5 kolonner i ListCtrl, med hver sit navn
        self.bestilling_list.InsertColumn(0, "Navn")
        self.bestilling_list.InsertColumn(1, "Pizza")
        self.bestilling_list.InsertColumn(2, "Burger")
        self.bestilling_list.InsertColumn(3, "Cola")
        self.bestilling_list.InsertColumn(4, "Pris i alt")
        #Indsæter et item, hvor det samlede antal af  mad og samlet pris, vil stå
        self.bestilling_list.InsertItem(0, "I alt")
        #Rækkefølgen som vores data indsættes i, når data indsættes i ListCtrl
        self.index = 0

        #Laver en liste, som skal bruges til at beregne samlet pris og antal mad
        self.iAlt = [0, 0, 0, 0]

        #Kører et for-loop, hvor den indsætter de dataer vi har fra listen(iAlt). hvor den indsætter dataerne i de forskellige spalter(0-4)
        for i in range(4):
            self.bestilling_list.SetItem(0, i + 1, str(self.iAlt[i]))


    def listener(self, bestilling):
        # Indsætter data i ListCtrl
        self.bestilling_list.InsertItem(self.index, bestilling[0])
        self.bestilling_list.SetItem(self.index, 1, str(bestilling[1]))
        self.bestilling_list.SetItem(self.index, 2, str(bestilling[2]))
        self.bestilling_list.SetItem(self.index, 3, str(bestilling[3]))
        self.bestilling_list.SetItem(self.index, 4, str(bestilling[4][:-2]) + ",- kr.")
        # Index stiger med 1, så de næste dataer ligger øverst.
        self.index += 1

        #Regner de madforbruget sammen, og indsætter det i ListCtrl
        for i in range(3):
            self.iAlt[i] += int(bestilling[i + 1])
            self.bestilling_list.SetItem(self.index, i + 1, str(self.iAlt[i]))

        #Tager den udregnede i alt pris, og indsætter i ListCtrl
        self.iAlt[3] += int(bestilling[4][:-2])
        self.bestilling_list.SetItem(self.index, 4, str(self.iAlt[3]) + ",- kr.")




    def listenervindue(self):
        #Lukker vindue
        self.Hide()