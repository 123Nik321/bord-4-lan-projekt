# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"LANparty Administration", pos = wx.DefaultPosition, size = wx.Size( 585,390 ), style = wx.DEFAULT_FRAME_STYLE|wx.MAXIMIZE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar2 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Udskriv rapport", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem1 )
		
		self.m_menuItem5 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"indstillinger", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem5 )
		
		self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem3 )
		
		self.m_menubar2.Append( self.m_menu1, u"File" ) 
		
		self.m_menu2 = wx.Menu()
		self.m_menuItem4 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Mad", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem4 )
		
		self.m_menuItem6 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Tilmelding", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem6 )
		
		self.m_menuItem7 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Turnering", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem7 )
		
		self.m_menubar2.Append( self.m_menu2, u"Edit" ) 
		
		self.SetMenuBar( self.m_menubar2 )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		
		self.SetSizer( bSizer2 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_MENU, self.OpenRapport, id = self.m_menuItem1.GetId() )
		self.Bind( wx.EVT_MENU, self.indstillinger, id = self.m_menuItem5.GetId() )
		self.Bind( wx.EVT_MENU, self.close, id = self.m_menuItem3.GetId() )
		self.Bind( wx.EVT_MENU, self.mad, id = self.m_menuItem4.GetId() )
		self.Bind( wx.EVT_MENU, self.tilmelding, id = self.m_menuItem6.GetId() )
		self.Bind( wx.EVT_MENU, self.OpenTurnering, id = self.m_menuItem7.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OpenRapport( self, event ):
		event.Skip()
	
	def indstillinger( self, event ):
		event.Skip()
	
	def close( self, event ):
		event.Skip()
	
	def mad( self, event ):
		event.Skip()
	
	def tilmelding( self, event ):
		event.Skip()
	
	def OpenTurnering( self, event ):
		event.Skip()
	

###########################################################################
## Class TilmeldingFrame
###########################################################################

class TilmeldingFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Tilmelding", pos = wx.Point( 50,100 ), size = wx.Size( 629,306 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		gSizer3 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText13 = wx.StaticText( self, wx.ID_ANY, u"Navn:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )
		gSizer3.Add( self.m_staticText13, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.tilmeld_Navn = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.tilmeld_Navn, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText14 = wx.StaticText( self, wx.ID_ANY, u"Hold:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14.Wrap( -1 )
		gSizer3.Add( self.m_staticText14, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.tilmeld_Hold = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.tilmeld_Hold, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText16 = wx.StaticText( self, wx.ID_ANY, u"Indtast hardware", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText16.Wrap( -1 )
		gSizer3.Add( self.m_staticText16, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.tilmeld_hardware = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.tilmeld_hardware, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText29 = wx.StaticText( self, wx.ID_ANY, u"Deltagere hele lan'et", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText29.Wrap( -1 )
		gSizer3.Add( self.m_staticText29, 0, wx.ALL, 5 )
		
		self.deltager_helelan = wx.CheckBox( self, wx.ID_ANY, u"Ja", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.deltager_helelan, 0, wx.ALL, 5 )
		
		self.m_staticText15 = wx.StaticText( self, wx.ID_ANY, u"Samlet forbrug i Watt:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )
		gSizer3.Add( self.m_staticText15, 0, wx.ALL, 5 )
		
		self.tilmeld_watforbrug = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.Point( 100,100 ), wx.DefaultSize, 0 )
		gSizer3.Add( self.tilmeld_watforbrug, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer7.Add( gSizer3, 0, wx.EXPAND, 5 )
		
		gSizer6 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_button6 = wx.Button( self, wx.ID_ANY, u"Tilmeld", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer6.Add( self.m_button6, 0, wx.ALL, 5 )
		
		self.m_button7 = wx.Button( self, wx.ID_ANY, u"Anuller", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer6.Add( self.m_button7, 0, wx.ALL, 5 )
		
		
		bSizer7.Add( gSizer6, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer7 )
		self.Layout()
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.anuller )
		self.m_button6.Bind( wx.EVT_BUTTON, self.tilmeld )
		self.m_button7.Bind( wx.EVT_BUTTON, self.anuller )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def anuller( self, event ):
		event.Skip()
	
	def tilmeld( self, event ):
		event.Skip()
	
	

###########################################################################
## Class MadFrame
###########################################################################

class MadFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Bestilling", pos = wx.Point( 100,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.STAY_ON_TOP|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.nametxt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
		self.nametxt.SetToolTipString( u"Navn" )
		
		bSizer3.Add( self.nametxt, 0, wx.ALL, 5 )
		
		gSizer1 = wx.GridSizer( 0, 3, 0, 0 )
		
		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Pizza", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		gSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.txtPizzaPris = wx.StaticText( self, wx.ID_ANY, u"45,- kr", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtPizzaPris.Wrap( -1 )
		gSizer1.Add( self.txtPizzaPris, 0, wx.ALL, 5 )
		
		pizza_choiceChoices = [ u"0", u"1", u"2", u"3", u"4", u"5" ]
		self.pizza_choice = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, pizza_choiceChoices, 0 )
		self.pizza_choice.SetSelection( 0 )
		gSizer1.Add( self.pizza_choice, 0, wx.ALL, 5 )
		
		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Burger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		gSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.txtBurgerPris = wx.StaticText( self, wx.ID_ANY, u"45,- kr.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtBurgerPris.Wrap( -1 )
		gSizer1.Add( self.txtBurgerPris, 0, wx.ALL, 5 )
		
		burger_choiseChoices = [ u"0", u"1", u"2", u"3", u"4", u"5" ]
		self.burger_choise = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, burger_choiseChoices, 0 )
		self.burger_choise.SetSelection( 0 )
		gSizer1.Add( self.burger_choise, 0, wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Cola", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		gSizer1.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.txtColaPris = wx.StaticText( self, wx.ID_ANY, u"12,- kr.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtColaPris.Wrap( -1 )
		gSizer1.Add( self.txtColaPris, 0, wx.ALL, 5 )
		
		cola_choiceChoices = [ u"0", u"1", u"2", u"3", u"4", u"5" ]
		self.cola_choice = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, cola_choiceChoices, 0 )
		self.cola_choice.SetSelection( 0 )
		gSizer1.Add( self.cola_choice, 0, wx.ALL, 5 )
		
		self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Samlet pris:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		self.m_staticText7.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		gSizer1.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		self.label_ialt = wx.StaticText( self, wx.ID_ANY, u"ialt", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.label_ialt.Wrap( -1 )
		self.label_ialt.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		gSizer1.Add( self.label_ialt, 0, wx.ALL, 5 )
		
		
		bSizer3.Add( gSizer1, 0, wx.EXPAND, 5 )
		
		self.m_button1 = wx.Button( self, wx.ID_ANY, u"Bestil", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button1, 0, wx.ALL, 5 )
		
		self.m_button2 = wx.Button( self, wx.ID_ANY, u"Afslut", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button2, 0, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()
		
		# Connect Events
		self.pizza_choice.Bind( wx.EVT_CHOICE, self.updateTotalPrice )
		self.burger_choise.Bind( wx.EVT_CHOICE, self.updateTotalPrice )
		self.cola_choice.Bind( wx.EVT_CHOICE, self.updateTotalPrice )
		self.m_button1.Bind( wx.EVT_BUTTON, self.bestil )
		self.m_button2.Bind( wx.EVT_BUTTON, self.slut_mad )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def updateTotalPrice( self, event ):
		event.Skip()
	
	
	
	def bestil( self, event ):
		event.Skip()
	
	def slut_mad( self, event ):
		event.Skip()
	

###########################################################################
## Class Oversigt_mad
###########################################################################

class Oversigt_mad ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Oversigt over madbestillinger", pos = wx.Point( 700,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.STAY_ON_TOP|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.bestilling_list = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT )
		bSizer4.Add( self.bestilling_list, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer4 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class Indstillinger
###########################################################################

class Indstillinger ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 100,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Priser", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		bSizer4.Add( self.m_staticText9, 0, wx.ALL, 5 )
		
		gSizer2 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText10 = wx.StaticText( self, wx.ID_ANY, u"Pizza", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText10.Wrap( -1 )
		gSizer2.Add( self.m_staticText10, 0, wx.ALL, 5 )
		
		self.pizza_pris = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.pizza_pris, 0, wx.ALL, 5 )
		
		self.m_staticText11 = wx.StaticText( self, wx.ID_ANY, u"Burger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		gSizer2.Add( self.m_staticText11, 0, wx.ALL, 5 )
		
		self.burger_pris = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.burger_pris, 0, wx.ALL, 5 )
		
		self.m_staticText12 = wx.StaticText( self, wx.ID_ANY, u"Cola", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )
		gSizer2.Add( self.m_staticText12, 0, wx.ALL, 5 )
		
		self.cola_pris = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.cola_pris, 0, wx.ALL, 5 )
		
		self.m_button3 = wx.Button( self, wx.ID_ANY, u"Gem", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_button3, 0, wx.ALL, 5 )
		
		self.m_button10 = wx.Button( self, wx.ID_ANY, u"Annuler", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_button10, 0, wx.ALL, 5 )
		
		
		bSizer4.Add( gSizer2, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer4 )
		self.Layout()
		
		# Connect Events
		self.m_button3.Bind( wx.EVT_BUTTON, self.close )
		self.m_button10.Bind( wx.EVT_BUTTON, self.AnnulerMadIndstil )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def close( self, event ):
		event.Skip()
	
	def AnnulerMadIndstil( self, event ):
		event.Skip()
	

###########################################################################
## Class TurneringsIndstillinger
###########################################################################

class TurneringsIndstillinger ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 700,100 ), size = wx.Size( 491,234 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer7 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText34 = wx.StaticText( self, wx.ID_ANY, u"Næste Fifa turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText34.Wrap( -1 )
		gSizer7.Add( self.m_staticText34, 0, wx.ALL, 5 )
		
		self.nextFifaIndstil = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer7.Add( self.nextFifaIndstil, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText35 = wx.StaticText( self, wx.ID_ANY, u"Næste Hearthstone turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText35.Wrap( -1 )
		gSizer7.Add( self.m_staticText35, 0, wx.ALL, 5 )
		
		self.nextHSIndstil = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer7.Add( self.nextHSIndstil, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText36 = wx.StaticText( self, wx.ID_ANY, u"Næste LOL turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText36.Wrap( -1 )
		gSizer7.Add( self.m_staticText36, 0, wx.ALL, 5 )
		
		self.nextLOLIndstil = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer7.Add( self.nextLOLIndstil, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.SaveBtn = wx.Button( self, wx.ID_ANY, u"Gem", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer7.Add( self.SaveBtn, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.CancelBtn = wx.Button( self, wx.ID_ANY, u"Annuler", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer7.Add( self.CancelBtn, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( gSizer7 )
		self.Layout()
		
		# Connect Events
		self.SaveBtn.Bind( wx.EVT_BUTTON, self.SaveTilmeldingIndstillinger )
		self.CancelBtn.Bind( wx.EVT_BUTTON, self.AnnulerTilmeldingIndstillinger )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def SaveTilmeldingIndstillinger( self, event ):
		event.Skip()
	
	def AnnulerTilmeldingIndstillinger( self, event ):
		event.Skip()
	

###########################################################################
## Class TurneringFrame
###########################################################################

class TurneringFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 100,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer5 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.txt12 = wx.StaticText( self, wx.ID_ANY, u"Hold navn", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txt12.Wrap( -1 )
		gSizer5.Add( self.txt12, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.TeamName = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.TeamName, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText18 = wx.StaticText( self, wx.ID_ANY, u"Spillere på holdet", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18.Wrap( -1 )
		gSizer5.Add( self.m_staticText18, 0, wx.ALL, 5 )
		
		self.TeamPlayers = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.TeamPlayers, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText19 = wx.StaticText( self, wx.ID_ANY, u"Vælg turnering", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText19.Wrap( -1 )
		gSizer5.Add( self.m_staticText19, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.FifaCB = wx.CheckBox( self, wx.ID_ANY, u"Fifa 19", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.FifaCB, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.HsCB = wx.CheckBox( self, wx.ID_ANY, u"Hearthstone", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.HsCB, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.LolCB = wx.CheckBox( self, wx.ID_ANY, u"League of legends", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.LolCB, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.TilmeldTurnering = wx.Button( self, wx.ID_ANY, u"Tilmeld", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.TilmeldTurnering, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.AnullerTurnering = wx.Button( self, wx.ID_ANY, u"Anuller", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer5.Add( self.AnullerTurnering, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( gSizer5 )
		self.Layout()
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.anuller )
		self.TilmeldTurnering.Bind( wx.EVT_BUTTON, self.TilMeldingTurnering )
		self.AnullerTurnering.Bind( wx.EVT_BUTTON, self.AnnullerTilmedling )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def anuller( self, event ):
		event.Skip()
	
	def TilMeldingTurnering( self, event ):
		event.Skip()
	
	def AnnullerTilmedling( self, event ):
		event.Skip()
	

###########################################################################
## Class Oversigt_turnering
###########################################################################

class Oversigt_turnering ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 700,100 ), size = wx.Size( 500,399 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.TurneringsList = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT )
		bSizer6.Add( self.TurneringsList, 1, wx.ALL|wx.EXPAND, 5 )
		
		gSizer8 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText28 = wx.StaticText( self, wx.ID_ANY, u"Næste Fifa turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText28.Wrap( -1 )
		gSizer8.Add( self.m_staticText28, 0, wx.ALL, 5 )
		
		self.NextFifaTour = wx.StaticText( self, wx.ID_ANY, u"Ændre dette i indstillinger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.NextFifaTour.Wrap( -1 )
		gSizer8.Add( self.NextFifaTour, 0, wx.ALL, 5 )
		
		self.m_staticText30 = wx.StaticText( self, wx.ID_ANY, u"Næste Hearthstone turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30.Wrap( -1 )
		gSizer8.Add( self.m_staticText30, 1, wx.ALL, 5 )
		
		self.NextHSTour = wx.StaticText( self, wx.ID_ANY, u"Ændre dette i indstillinger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.NextHSTour.Wrap( -1 )
		gSizer8.Add( self.NextHSTour, 0, wx.ALL, 5 )
		
		self.m_staticText32 = wx.StaticText( self, wx.ID_ANY, u"Næste LOL turnering:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText32.Wrap( -1 )
		gSizer8.Add( self.m_staticText32, 0, wx.ALL, 5 )
		
		self.NextLOLTour = wx.StaticText( self, wx.ID_ANY, u"Ændre dette i indstillinger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.NextLOLTour.Wrap( -1 )
		gSizer8.Add( self.NextLOLTour, 0, wx.ALL, 5 )
		
		
		bSizer6.Add( gSizer8, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer6 )
		self.Layout()
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.anuller )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def anuller( self, event ):
		event.Skip()
	

###########################################################################
## Class RapportFrame
###########################################################################

class RapportFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 498,145 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer9 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_button11 = wx.Button( self, wx.ID_ANY, u"Udskriv rapport", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer9.Add( self.m_button11, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_button12 = wx.Button( self, wx.ID_ANY, u"Annuler", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer9.Add( self.m_button12, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( gSizer9 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button11.Bind( wx.EVT_BUTTON, self.WriteRapport )
		self.m_button12.Bind( wx.EVT_BUTTON, self.AnnulerRapport )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def WriteRapport( self, event ):
		event.Skip()
	
	def AnnulerRapport( self, event ):
		event.Skip()
	

###########################################################################
## Class Oversigt_Tilmelding
###########################################################################

class Oversigt_Tilmelding ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 700,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.TilmedlingsList = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT )
		bSizer6.Add( self.TilmedlingsList, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer6 )
		self.Layout()
	
	def __del__( self ):
		pass
	

