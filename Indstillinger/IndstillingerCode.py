from Andet import gui
import pickle

class indstillinger(gui.Indstillinger):
    def __init__(self, parent):
        gui.Indstillinger.__init__(self, parent)
        self.config = []
        #Filnavn til hvor data skal gemmes
        self.filename = "Andet\config.ini"

    def close( self, event ):
        #Tilføjer data til listen(config)
        self.config.append(self.pizza_pris.GetValue())
        self.config.append(self.burger_pris.GetValue())
        self.config.append(self.cola_pris.GetValue())
        #wb = write binary, åbner filen filename.
        outfile = open(self.filename, "wb")
        #Indsætter data ind i filen(filename)
        pickle.dump(self.config, outfile)
        #Lukker filen, og lukker vinduet
        outfile.close()
        self.Hide()

    def AnnulerMadIndstil( self, event ):
        #Lukker vindue
        self.Hide()