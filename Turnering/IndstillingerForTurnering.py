from Andet import gui
from wx.lib.pubsub import pub
import pickle

class turneringsIndstillingerFrame(gui.TurneringsIndstillinger):
    def __init__(self, parent):
        gui.TurneringsIndstillinger.__init__(self, parent)
        self.config = []
        self.filename = "Andet\configNextTur.ini"
        



    def SaveTilmeldingIndstillinger( self, event ):
        #Indsætter tilmeldingsdataer i en liste, ved navn config
        self.config.append(self.nextFifaIndstil.GetValue())
        self.config.append(self.nextLOLIndstil.GetValue())
        self.config.append(self.nextHSIndstil.GetValue())

        #wb = write binary
        outfile = open(self.filename, "wb")
        #indsætter data i config fil
        pickle.dump(self.config, outfile)
        outfile.close()
        #lukker vindue
        self.Hide()

    def AnnulerTilmeldingIndstillinger( self, event ):
        self.Hide()
