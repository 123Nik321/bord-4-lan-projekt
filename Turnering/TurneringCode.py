from Andet import gui
from wx.lib.pubsub import pub
import csv

class turneringsFrame(gui.TurneringFrame):
    def __init__(self, parent):
        gui.TurneringFrame.__init__(self, parent)
        self.TilmeldingList = []
        self.fileName = "CsvFiler\RapportTurnering.csv"



    def TilMeldingTurnering(self, event):

        #Laver liste
        self.TilmeldingListTilRapport = []
        self.TilmeldingListTilRapport.append("Holdnavn: " + self.TeamName.GetValue())
        self.TilmeldingListTilRapport.append("Spillere på holdet: " + self.TeamPlayers.GetValue())

        #Tilføjer data til TilmeldingList og tjekker længere nede, om man deltager eller ej, i de diverse turneringer
        self.TilmeldingList.append(self.TeamName.GetValue())
        self.TilmeldingList.append(self.TeamPlayers.GetValue())

        if self.FifaCB.IsChecked():
            self.TilmeldingList.append("Deltager")
            self.TilmeldingListTilRapport.append("Deltager i Fifa turnering")
        else:
            self.TilmeldingList.append("Deltager ikke")
            self.TilmeldingListTilRapport.append("Deltager ikke i Fifa turnering")

        if self.LolCB.IsChecked():
            self.TilmeldingList.append("Deltager")
            self.TilmeldingListTilRapport.append("Deltager i LOL turnering")
        else:
            self.TilmeldingList.append("Deltager ikke")
            self.TilmeldingListTilRapport.append("Deltager ikke i LOL turnering")

        if self.HsCB.IsChecked():
            self.TilmeldingList.append("Deltager")
            self.TilmeldingListTilRapport.append("Deltager i Hearthstone turnering")
        else:
            self.TilmeldingList.append("Deltager ikke")
            self.TilmeldingListTilRapport.append("Deltager ikke i Hearthstone turnering")

        #Sender listen(TilmeldingsList)
        pub.sendMessage("tilmelding", tilmelding=self.TilmeldingList)

        with open(self.fileName, mode='a') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            csv_writer.writerow(self.TilmeldingListTilRapport)

        #Rydder listen
        self.TilmeldingList.clear()



    def AnnullerTilmedling( self, event ):
        #Sender besked om at lukke vindue
        pub.sendMessage("afslutTilm")
        #Lukker vindue
        self.Hide()
