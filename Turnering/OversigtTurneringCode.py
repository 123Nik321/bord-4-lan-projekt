from Andet import gui
from wx.lib.pubsub import pub
import pickle


class turnerings_oversigtFrame(gui.Oversigt_turnering):
    def __init__(self, parent):
        gui.Oversigt_turnering.__init__(self, parent)
        #Hvis programmet modtager afslutTilm, kører den funktionen listenervindue
        pub.subscribe(self.listenervindue, "afslutTilm")
        # Hvis programmet modtager tilmelding, kører den funktionen Tillistener
        pub.subscribe(self.Tillistener, "tilmelding")
        #Laver 5 kolonner i ListCtrl, med hver sit navn
        self.TurneringsList.InsertColumn(0, "Holdnavn")
        self.TurneringsList.InsertColumn(1, "Spillere")
        self.TurneringsList.InsertColumn(2, "Fifa 19")
        self.TurneringsList.InsertColumn(3, "LOL")
        self.TurneringsList.InsertColumn(4, "HS")
        #Rækkefølgen som vores data indsættes i, når data indsættes i ListCtrl
        self.indexTil = 0

        self.config = []
        self.filename = "Andet\configNextTur.ini"
        #rb = read binary. Åbnner filen med navnet filename(Som er skrevet ovenfor)
        infile = open(self.filename, "rb")
        #Læser filen, og indsætter dataen ind i listen ved navn config.
        self.config = pickle.load(infile)
        #Lukker filen
        infile.close()

        #Tager data nr. 1 i listen self.config
        self.NextFifaTour.SetLabelText(self.config[0])

        # Tager data nr. 2 i listen self.config
        self.NextLOLTour.SetLabelText(self.config[1])

        # Tager data nr. 3 i listen self.config
        self.NextHSTour.SetLabelText(self.config[2])



    def Tillistener(self, tilmelding):
        #Indsætter data i ListCtrl
        self.TurneringsList.InsertItem(self.indexTil, tilmelding[0])
        self.TurneringsList.SetItem(self.indexTil, 1, str(tilmelding[1]))
        self.TurneringsList.SetItem(self.indexTil, 2, str(tilmelding[2]))
        self.TurneringsList.SetItem(self.indexTil, 3, str(tilmelding[3]))
        self.TurneringsList.SetItem(self.indexTil, 4, str(tilmelding[4]))
        #Index stiger med 1, så de næste dataer ligger øverst.
        self.indexTil += 1

    def listenervindue(self):
        #Lukker vindue vindue
        self.Hide()

