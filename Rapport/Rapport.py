from Andet import gui
import csv
import os
import pickle

class RapportForTurnering(gui.RapportFrame):
    def __init__(self, parent):
        gui.RapportFrame.__init__(self, parent)


    def AnnulerRapport( self, event ):
        #Lukker vindue
        self.Hide()

    def WriteRapport( self, event ):
        "Giver filnavn"
        fileNameTilmeld = "CsvFiler\RapportTilmelding.csv"
        fileNameMad = "CsvFiler\RapportMad.csv"
        fileNameTurnering = "CsvFiler\RapportTurnering.csv"
        filename = "RapportFil.txt"

        #w = write, åbner filen
        Rapport = open(filename, "w")

        #TIlskrover noget til filen
        Rapport.write("Tilmeldinger\n")

        #Åbner ny fil som csv_file
        with open(fileNameTilmeld, 'r') as csv_file:
            #Læser filen
            csv_reader = csv.reader(csv_file)


            #Kører igennem hver linje i filen, og indsætter linjerne inde i txt filen.
            for line in csv_reader:
                Rapport.write(str(line).replace("[", "").replace("]", "").replace('"', "").replace("'", "") + "\n")


        #Starter forfra og laver det samme med mad
        Rapport.write("\n\nMad\n")

        with open(fileNameMad, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)

            for line in csv_reader:
                Rapport.write(str(line).replace("[", "").replace("]", "").replace('"', "").replace("'", "") + "\n")


        # Starter forfra og laver det samme med turnering
        Rapport.write("\n\nTurnering\n")

        with open(fileNameTurnering, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)



            for line in csv_reader:
                Rapport.write(str(line).replace("[", "").replace("]", "").replace('"', "").replace("'", "") + "\n")

        self.config = []
        self.filename = "Andet\configNextTur.ini"
        # rb = read binary. Åbnner filen med navnet filename(Som er skrevet ovenfor)
        infile = open(self.filename, "rb")
        # Læser filen, og indsætter dataen ind i listen ved navn config.
        self.config = pickle.load(infile)
        # Lukker filen
        infile.close()

        # Tager data nr. 1 i listen self.config
        Rapport.write("\nDato for Fifa turnering: " + self.config[0] + "\n\n")

        # Tager data nr. 2 i listen self.config
        Rapport.write("Dato for LOL turnering: " + self.config[1] + "\n\n")

        # Tager data nr. 3 i listen self.config
        Rapport.write("Dato for HearthStone turnering: " + self.config[2])

        Rapport.close()

        #Kører txt filen
        os.startfile(str(filename))
